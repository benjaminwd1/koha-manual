.. include:: images.rst

.. _whats-new-label:

What's new in Koha
===============================================================================

This section highlights new features and enhancements in the latest Koha
releases.

This section is updated as the manual is updated. Therefore, it may not contain
all the new features and enhancements in the release. Please consult the release
notes to find an exhaustive list of all changes in Koha for each version.

.. _whats-new-23-11-label:

23.11
-------------------------------------------------------------------------------

.. Tip::

   `Read the full release notes for Koha 23.11.00 <https://koha-community.org/koha-23-11-released/>`_.

.. _whats-new-23-11-preservation:

Preservation module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The brand new :ref:`Preservation module <preservation-label>` is used for integrating
preservation treatments into the Koha workflow and keep track of them. For every single
step of the preservation workflow, data is attached to the Koha items.

The module comes with its own set of :ref:`system preferences<preservation-system-preferences-label>`.

.. _whats-new-23-11-protected-patrons-label:

Protected patrons
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is now possible to protect patrons from deletion. When
:ref:`adding <add-a-new-patron-label>` or
:ref:`editing a patron <editing-patrons-label>`, a new 'Protected' flag can be
set in the 'Library management' section. This will disable the 'Delete' option
in the patron file. Furthermore, protected patrons cannot be deleted by batch
deletion, cron jobs, or patron merging.

Use this for your statistical patrons, SIP2 users, self checkout users and
superadmins.

.. _whats-new-23-11-vendor-issues-label:

Vendor issues
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is now possible to :ref:`record problems with vendors <vendor-issues-label>`
in the acquisitions module. It is a way to keep track of the various issues that
might arise in the course of a contract, and it might be helpful when the time
comes to renegociate.

.. _whats-new-23-11-html-customization-new-locations-label:

New display locations for HTML customizations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are several new display locations for the
:ref:`HTML customization <html-customizations-label>` tool.

-  StaffAcquisitionsHome: content will appear at the bottom of the
   :ref:`Acquisitions module <acquisitions-label>` main page.

-  StaffAuthoritiesHome: content will appear at the bottom of the
   :ref:`Authorities <authorities-label>` main page.

-  StaffCataloguingHome: content will appear at the bottom of the
   :ref:`Cataloguing module <cataloging-label>` main page.

-  StaffListsHome: content will appear at the bottom of the
   :ref:`Lists <lists-label>` main page.

-  StaffPatronsHome: content will appear at the bottom of the
   :ref:`Patrons module <patrons-label>` main page.

-  StaffPOSHome: content will appear at the bottom of the
   :ref:`Point of sale <point-of-sale-label>` main page.

-  StaffSerialsHome: content will appear at the bottom of the
   :ref:`Serials module <serials-label>` main page.

The move of system preferences to HTML customizations continues. These were
moved to the :ref:`HTML customization tool <html-customizations-label>` in
version 23.11.

-  OpacMaintenanceNotice

-  OPACResultsSidebar

-  OpacSuppressionMessage

-  PatronSelfRegistrationAdditionalInstructions

-  SCOMainUserBlock

-  SelfCheckHelpMessage

-  SelfCheckInMainUserBlock

.. _whats-new-23-11-sysprefs-label:

New system preferences
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**AcquisitionsDefaultEMailAddress**

The new :ref:`AcquisitionsDefaultEMailAddress <acquisitionsdefaultemailaddress-label>`
allows you to set a specific email address that will be used to send orders and
late order claims from the acquisitions module.

**AcquisitionsDefaultReplyTo**

The new :ref:`AcquisitionsDefaultReplyTo <acquisitionsdefaultreplyto-label>`
allows you to set a specific reply-to email address that will receive replies to
orders and late order claims sent from the acquisitions module.

**DefaultAuthorityTab**

The new :ref:`DefaultAuthorityTab <defaultauthoritytab-label>` system preference
allows libraries to choose which tab is selected first when viewing an authority
record.

**ForceLibrarySelection**

The new :ref:`ForceLibrarySelection <forcelibraryselection-label>` can be used
to require staff to choose a library when logging into the staff interface.

**SerialsDefaultEMailAddress**

The new :ref:`SerialsDefaultEMailAddress <serialsdefaultemailaddress-label>`
allows you to set a specific email address that will be used to send late serial
issues claims from the serials module.

**SerialsDefaultReplyTo**

The new :ref:`SerialsDefaultReplyTo <serialsdefaultreplyto-label>`
allows you to set a specific reply-to email address that will receive replies to
late serial issues claims sent from the serials module.

**showLastPatronCount**

The new :ref:`showLastPatronCount <showlastpatroncount-label>` system preference
allows you to choose how many patrons are shown by the link created by
:ref:`showLastPatron <showLastPatron-label>`.

**TrackLastPatronActivityTriggers**

The new :ref:`TrackLastPatronActivityTriggers <tracklastpatronactivitytriggers-label>`
system preference replaces the
:ref:`TrackLastPatronActivity <tracklastpatronactivity-label>` system preference
and allows a more granular control of which action triggers the update of the
patron's "last seen" date (borrowers.lastseen). Previously, this database column
was only updated when the patron logged into the OPAC or via SIP2. But this
excluded patrons who might check out a lot of items from the library, but never
log into the OPAC. The library can now decide which activities to track from a
list.